FROM --platform=amd64 openjdk:17
WORKDIR /app
COPY ./opentelemetry-javaagent.jar .
COPY ./target/*.jar ./petclinic.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "./petclinic.jar"]
